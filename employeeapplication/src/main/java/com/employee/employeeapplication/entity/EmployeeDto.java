package com.employee.employeeapplication.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

public class EmployeeDto implements Serializable{

	private static final long serialVersionUID = 1L;

	@JsonAlias("first_name")
	private String firstName;
	
	@JsonAlias("last_name")
	private String lastName;
	
	@JsonAlias("joining_date")
	private String joiningDate;
	
	@JsonAlias("location")
	private String location;
	
	@JsonAlias("experience")
	private String experience;
	
	@JsonAlias("isManager")
	private String isManager;
	
	public String getIsManager() {
		return isManager;
	}

	public void setIsManager(String isManager) {
		this.isManager = isManager;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
