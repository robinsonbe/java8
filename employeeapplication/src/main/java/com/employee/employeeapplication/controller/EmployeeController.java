package com.employee.employeeapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.employee.employeeapplication.entity.EmployeeDto;
import com.employee.employeeapplication.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@PostMapping("/saveEmployee")
	public String saveEmployee(@RequestBody EmployeeDto empDto) {
		employeeService.saveEmployee(empDto);
		return "Saved";
	}
	
	@GetMapping("/getEmployeesBasedOnJoiningDate")
	public List<EmployeeDto> getEmployees(@RequestParam String joiningDate) {
		List<EmployeeDto> empList = employeeService.getEmployees(joiningDate);
		return empList;
	}
	
	@GetMapping("/topFiveEmployees")
	public List<EmployeeDto> getEmployees() {
		List<EmployeeDto> empList = employeeService.getEmployeesSorted();
		return empList;
	}
	
	@GetMapping("/getEmployeesWithExperience")
	public List<EmployeeDto> getEmployeesWithExperience() {
		List<EmployeeDto> empList = employeeService.getEmployeesWithExperience();
		return empList;
	}
}
