package com.employee.employeeapplication.service;

import java.util.List;

import com.employee.employeeapplication.entity.EmployeeDto;

public interface EmployeeService {
	
	List<EmployeeDto> getEmployeesSorted();

	List<EmployeeDto> getEmployees(String joiningDate);

	void saveEmployee(EmployeeDto empDto);

	List<EmployeeDto> getEmployeesWithExperience();

}
