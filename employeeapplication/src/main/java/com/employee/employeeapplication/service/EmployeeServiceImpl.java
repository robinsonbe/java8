package com.employee.employeeapplication.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.employeeapplication.entity.EmployeeDto;
import com.employee.employeeapplication.model.EmployeeInfo;
import com.employee.employeeapplication.repositoy.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService{
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Override
	public List<EmployeeDto> getEmployees(String joiningDate) {
		List<EmployeeDto> empList = new ArrayList<EmployeeDto>();
		List<EmployeeInfo> empInfos;
		empInfos = employeeRepository.findAll();
		if(!empInfos.isEmpty()) {
			LocalDate date = null;
			try {
				date = LocalDate.parse(joiningDate);
			} catch(Exception e) {
				return empList;
			}
			List<EmployeeDto> empListTemp = empInfos.stream().map(emp ->mappingToDto(emp)).collect(Collectors.toList());
			for(EmployeeDto emp: empListTemp) {
				LocalDate availableDate = LocalDate.parse(emp.getJoiningDate());
				if(date.isEqual(availableDate)) {
					empList.add(emp);
				}
			}			
		}
		return empList;
	}
	
	@Override
	public List<EmployeeDto> getEmployeesSorted() {
		List<EmployeeDto> empList = new ArrayList<EmployeeDto>();
		List<EmployeeInfo> empInfos;
		empInfos = employeeRepository.findAll();
		if(!empInfos.isEmpty()) {
			List<EmployeeDto> empListTemp = empInfos.stream().map(emp ->mappingToDto(emp)).collect(Collectors.toList());
			Collections.sort(empListTemp, Comparator.comparing(EmployeeDto::getJoiningDate));
			empList = empListTemp.stream().limit(5).collect(Collectors.toList());			
		}
		return empList;
	}

	private EmployeeDto mappingToDto(EmployeeInfo emp) {
		EmployeeDto employeeDto = new EmployeeDto();
		employeeDto.setFirstName(emp.getFirstName());
		employeeDto.setLastName(emp.getLastName());
		employeeDto.setLocation(emp.getLocation());
		employeeDto.setExperience(emp.getExperience());
		if(emp.getRole().equalsIgnoreCase("Manager")) {
			employeeDto.setIsManager("yes");
		} else if (emp.getRole().equalsIgnoreCase("Regular")) {
			employeeDto.setIsManager("no");
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dateFormat.format(emp.getJoiningDate());
		employeeDto.setJoiningDate(dateStr);
		return employeeDto;
	}

	@Override
	public void saveEmployee(EmployeeDto empDto) {
		EmployeeInfo employeeInfo = new EmployeeInfo();
		employeeInfo.setFirstName(empDto.getFirstName());
		employeeInfo.setLastName(empDto.getLastName());
		employeeInfo.setLocation(empDto.getLocation());
		employeeInfo.setExperience(empDto.getExperience());
		if(empDto.getIsManager().equalsIgnoreCase("yes")) {
			employeeInfo.setRole("Manager");
		} else if (empDto.getIsManager().equalsIgnoreCase("no")) {
			employeeInfo.setRole("Regular");
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			employeeInfo.setJoiningDate(formatter.parse(empDto.getJoiningDate()));
		} catch (ParseException e) {
		}
		employeeRepository.save(employeeInfo);
	}

	@Override
	public List<EmployeeDto> getEmployeesWithExperience() {
		List<EmployeeDto> empList = new ArrayList<EmployeeDto>();
		List<EmployeeInfo> empInfos;
		empInfos = employeeRepository.findAll();
		if(!empInfos.isEmpty()) {
			List<EmployeeDto> empListTemp = empInfos.stream().map(emp ->mappingToDto(emp)).collect(Collectors.toList());
			empList = empListTemp.stream().filter(emp -> Integer.valueOf(emp.getExperience()) > 7).collect(Collectors.toList());			
		}
		return empList;
	}

}
