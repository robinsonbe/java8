package com.order.foodorder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.foodorder.model.FoodOrder;

@Repository
public interface FoodOrderRepository extends JpaRepository<FoodOrder, Long>{

	List<FoodOrder> findByItemNameContains(String itemName);

	FoodOrder findByItemId(Long itemId);

}
