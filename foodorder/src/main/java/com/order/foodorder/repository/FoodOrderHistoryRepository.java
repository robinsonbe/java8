package com.order.foodorder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.foodorder.model.OrderDetails;

@Repository
public interface FoodOrderHistoryRepository extends JpaRepository<OrderDetails, Integer>{

}
