package com.order.foodorder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.order.foodorder.entity.FoodOrderReceipt;
import com.order.foodorder.entity.FoodOrderReqVO;
import com.order.foodorder.entity.FoodOrderResVO;
import com.order.foodorder.entity.FoodOrderVO;
import com.order.foodorder.entity.OrderHistoryResVO;
import com.order.foodorder.service.FoodOrderService;

@RestController
@RequestMapping("/order")
public class FoodOrderController {
	
	@Autowired
	FoodOrderService foodOrderService;
	
	@GetMapping("/checkItems")
	public List<FoodOrderResVO> checkitems(@RequestParam String itemName) {	
		List<FoodOrderResVO> result = foodOrderService.checkitems(itemName);
		return result;
	}
	
	@PostMapping(value = "/orderFood", consumes = "application/json")
	public List<FoodOrderReceipt> orderFood(@RequestBody List<FoodOrderReqVO> foodOrderVO, @RequestParam String fromAccNum) {	
		List<FoodOrderReceipt> result = foodOrderService.orderFood(foodOrderVO, fromAccNum);
		return result;
	}
	
	@GetMapping("/orderHistory")
	public List<OrderHistoryResVO> orderHistory() {	
		List<OrderHistoryResVO> result = foodOrderService.orderHistory();
		return result;
	}

}
