package com.order.foodorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.order.foodorder.config.RibbonConfiguration;

@EnableEurekaClient
@SpringBootApplication
@EnableFeignClients
//@RibbonClient(name = "paymentClient", configuration = RibbonConfiguration.class)
public class FoodorderApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodorderApplication.class, args);
	}

}
