package com.order.foodorder.service;

import java.util.List;

import com.order.foodorder.entity.FoodOrderReceipt;
import com.order.foodorder.entity.FoodOrderReqVO;
import com.order.foodorder.entity.FoodOrderResVO;
import com.order.foodorder.entity.FoodOrderVO;
import com.order.foodorder.entity.OrderHistoryResVO;

public interface FoodOrderService {

	List<FoodOrderResVO> checkitems(String itemName);

	List<FoodOrderReceipt> orderFood(List<FoodOrderReqVO> foodOrderVO, String fromAccNum);

	List<OrderHistoryResVO> orderHistory();

}
