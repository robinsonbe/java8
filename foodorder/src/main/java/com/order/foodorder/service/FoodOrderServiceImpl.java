package com.order.foodorder.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.order.foodorder.entity.FoodOrderReceipt;
import com.order.foodorder.entity.FoodOrderReqVO;
import com.order.foodorder.entity.FoodOrderResVO;
import com.order.foodorder.entity.FoodOrderVO;
import com.order.foodorder.entity.OrderHistoryResVO;
import com.order.foodorder.feignclient.BankAppClient;
import com.order.foodorder.model.FoodOrder;
import com.order.foodorder.model.OrderDetails;
import com.order.foodorder.repository.FoodOrderHistoryRepository;
import com.order.foodorder.repository.FoodOrderRepository;

@Service
public class FoodOrderServiceImpl implements FoodOrderService {

	@Autowired
	FoodOrderRepository foodOrderRepository;

	@Autowired
	FoodOrderHistoryRepository foodOrderHistoryRepository;

	@Autowired
	BankAppClient bankAppClient;
	
	static Map<String, String> vendorAccMap = new HashMap<String, String>();
	
	static {
		vendorAccMap.put("shop1", "1063399033");
		vendorAccMap.put("shop2", "2026240484");
	}

	@Override
	public List<FoodOrderResVO> checkitems(String itemName) {
		List<FoodOrderResVO> foodOrderResVOs = new ArrayList<FoodOrderResVO>();
		List<FoodOrder> results = foodOrderRepository.findByItemNameContains(itemName);
		if (!results.isEmpty()) {
			for (FoodOrder foodOrder : results) {
				FoodOrderResVO resVO = new FoodOrderResVO();
				resVO.setItemId(String.valueOf(foodOrder.getItemId()));
				resVO.setItemName(foodOrder.getItemName());
				resVO.setVendorName(foodOrder.getVendorName());
				resVO.setQuantityAvailable(foodOrder.getQuantityAvailable());
				resVO.setPrice(foodOrder.getPrice());
				foodOrderResVOs.add(resVO);
			}
		}
		return foodOrderResVOs;
	}

	@Override
	public List<FoodOrderReceipt> orderFood(List<FoodOrderReqVO> foodOrderVOs, String fromAccNum) {
		List<FoodOrderReceipt> foodOrderResVOs = new ArrayList<FoodOrderReceipt>();
		for (FoodOrderReqVO foodOrderVO : foodOrderVOs) {
			FoodOrder results = foodOrderRepository.findByItemId(Long.valueOf(foodOrderVO.getItemId()));
			if (results != null) {
				int availQty = Integer.valueOf(results.getQuantityAvailable())
						- Integer.valueOf(foodOrderVO.getOrderQty());
				results.setQuantityAvailable(String.valueOf(availQty));
				foodOrderRepository.save(results);
			}
			OrderDetails orderHistory = new OrderDetails();
			int orderid = genOrderId();
			String orderIdTmp = String.valueOf(orderid).replaceAll("-", "");
			orderHistory.setOrderId(Integer.valueOf(orderIdTmp));
			orderHistory.setItemId(foodOrderVO.getItemId());
			orderHistory.setItemName(foodOrderVO.getItemName());
			orderHistory.setPrice(foodOrderVO.getPrice());
			orderHistory.setQuantity(foodOrderVO.getOrderQty());
			orderHistory.setVendorName(foodOrderVO.getVendorName());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			Date date = new Date();
			String dateStr = formatter.format(date);
			try {
				orderHistory.setUpdatedDate(formatter.parse(dateStr));
			} catch (ParseException e) {
			}
			foodOrderHistoryRepository.save(orderHistory);
		}
		String totalAmount = getTotalAmount(foodOrderVOs);
		FoodOrderReceipt receipt = new FoodOrderReceipt();
		receipt.setOrderDetails(foodOrderVOs);
		receipt.setTotalAmount(totalAmount);
		foodOrderResVOs.add(receipt);
		String paymentStatus = bankAppClient.completePayment(fromAccNum, "1063399033", totalAmount);
		return foodOrderResVOs;
	}

	private String getTotalAmount(List<FoodOrderReqVO> foodOrderVOs) {
		String totalStr = "0";
		int total = 0;
		for (FoodOrderReqVO foodOrderVO : foodOrderVOs) {
			total += Integer.valueOf(foodOrderVO.getPrice()) * Integer.valueOf(foodOrderVO.getOrderQty());
		}
		totalStr = String.valueOf(total);
		return totalStr;
	}

	@Override
	public List<OrderHistoryResVO> orderHistory() {
		List<OrderHistoryResVO> orderHistoryResVOs = new ArrayList<OrderHistoryResVO>();
		List<OrderDetails> orderListTemp = foodOrderHistoryRepository.findAll(Sort.by("updatedDate").descending());
		List<OrderDetails> orderList = orderListTemp.stream().limit(5).collect(Collectors.toList());
		for (OrderDetails orderDetail : orderList) {
			OrderHistoryResVO resVO = new OrderHistoryResVO();
			resVO.setOrderId(orderDetail.getOrderId());
			resVO.setItemId(orderDetail.getItemId());
			resVO.setItemName(orderDetail.getItemName());
			resVO.setPrice(orderDetail.getPrice());
			resVO.setOrderQty(orderDetail.getQuantity());
			resVO.setVendorName(orderDetail.getVendorName());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			resVO.setOrderTime(formatter.format(orderDetail.getUpdatedDate()));
			orderHistoryResVOs.add(resVO);
		}
		return orderHistoryResVOs;
	}
	
	public int genOrderId() {
	    Random r = new Random(System.currentTimeMillis());
	    return 1000000000 + r.nextInt(2000000000);
	}

}
