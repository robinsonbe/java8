package com.order.foodorder.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FoodOrderVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonAlias("itemName")
	private String itemName;

}
