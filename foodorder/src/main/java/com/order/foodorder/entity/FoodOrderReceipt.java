package com.order.foodorder.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FoodOrderReceipt implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	@JsonAlias("orderDetails")
	private List<FoodOrderReqVO> orderDetails;
	
	@JsonAlias("totalAmount")
	private String totalAmount;

	public List<FoodOrderReqVO> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<FoodOrderReqVO> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	

}
