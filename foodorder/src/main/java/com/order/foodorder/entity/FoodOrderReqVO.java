package com.order.foodorder.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FoodOrderReqVO implements Serializable{

	private static final long serialVersionUID = 1L;

	@JsonAlias("itemName")
	private String itemName;

	@JsonAlias("itemId")
	private String itemId;
	
	@JsonAlias("vendorName")
	private String vendorName;
	
	@JsonAlias("price")
	private String price;
	
	@JsonAlias("orderQty")
	private String orderQty;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(String orderQty) {
		this.orderQty = orderQty;
	}

}
