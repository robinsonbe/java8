package com.order.foodorder.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

public class OrderHistoryResVO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonAlias("orderId")
	private int orderId;
	
	@JsonAlias("itemName")
	private String itemName;

	@JsonAlias("itemId")
	private String itemId;
	
	@JsonAlias("vendorName")
	private String vendorName;
	
	@JsonAlias("price")
	private String price;
	
	@JsonAlias("orderQty")
	private String orderQty;
	
	@JsonAlias("orderTime")
	private String orderTime;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(String orderQty) {
		this.orderQty = orderQty;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

}
