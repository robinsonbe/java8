package com.bank.bankapplication.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.bankapplication.entity.Account;
import com.bank.bankapplication.entity.User;
import com.bank.bankapplication.model.AccountDetails;
import com.bank.bankapplication.model.UserInfo;
import com.bank.bankapplication.repository.BankApplicationRepository;
import com.bank.bankapplication.repository.FundTransferRepository;

@Service
public class BankApplicationServiceImpl implements BankApplicationService {

	@Autowired
	BankApplicationRepository bankApplicationRepository;

	@Autowired
	FundTransferRepository fundTransferRepository;

	@Override
	public void registerUser(User usr) {
		UserInfo userInfo = new UserInfo();
		userInfo.setAccountNumber(usr.getAccountNumber());
		userInfo.setFirstName(usr.getFirstName());
		userInfo.setLastName(usr.getLastName());
		userInfo.setLocation(usr.getLocation());
		userInfo.setIdentityDocumentNumber(usr.getIdentityDocumentNumber());
		bankApplicationRepository.save(userInfo);
	}

	@Override
	public void addFunds(String accountNumber, String amount) {
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountNumber(accountNumber);
		accountDetails.setAccountBalance(amount);
		accountDetails.setAmount(amount);
		accountDetails.setTransactionType("Credit");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date date = new Date();
		String dateStr = formatter.format(date);
		try {
			accountDetails.setUpdatedDate(formatter.parse(dateStr));
		} catch (ParseException e) {
		}
		fundTransferRepository.save(accountDetails);
	}

	@Override
	public void transferFunds(String fromAccountNumber, String toAccountNumber, String amount) {
		List<AccountDetails> fromAccountDetailsRes = fundTransferRepository.findByAccountNumberOrderByAccountBalance(fromAccountNumber);
		AccountDetails fromAccountDetails = fromAccountDetailsRes.get(0);
		if (!fromAccountDetails.equals(null)) {
			int fromAccountBalance = Integer.valueOf(fromAccountDetails.getAccountBalance());
			int amountInt = Integer.valueOf(amount);
			fromAccountBalance = fromAccountBalance - amountInt;

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			Date date = new Date();
			String dateStr = formatter.format(date);

			AccountDetails fromAccountDetailsNew = new AccountDetails();
			fromAccountDetailsNew.setAccountNumber(fromAccountDetails.getAccountNumber());
			fromAccountDetailsNew.setAccountBalance(String.valueOf(fromAccountBalance));
			fromAccountDetailsNew.setAmount(amount);
			fromAccountDetailsNew.setTransactionType("Debit");
			try {
				fromAccountDetailsNew.setUpdatedDate(formatter.parse(dateStr));
			} catch (ParseException e) {
			}

			fundTransferRepository.save(fromAccountDetailsNew);

			List<AccountDetails> toAccountDetailsRes = fundTransferRepository.findByAccountNumberOrderByAccountBalanceDesc(toAccountNumber);
			AccountDetails toAccountDetails = toAccountDetailsRes.get(0);
			int toAccountBalance = Integer.valueOf(toAccountDetails.getAccountBalance());
			toAccountBalance = toAccountBalance + amountInt;
			AccountDetails toAccountDetailsNew = new AccountDetails();
			toAccountDetailsNew.setAccountNumber(toAccountDetails.getAccountNumber());
			toAccountDetailsNew.setAccountBalance(String.valueOf(toAccountBalance));
			toAccountDetailsNew.setAmount(amount);
			toAccountDetailsNew.setTransactionType("Credit");
			try {
				toAccountDetailsNew.setUpdatedDate(formatter.parse(dateStr));
			} catch (ParseException e) {
			}
			fundTransferRepository.save(toAccountDetailsNew);
		}
	}

	@Override
	public List<Account> generateReport(String accountNum, String fromDate, String toDate) {
		List<Account> accounts = new ArrayList<Account>();
		List<AccountDetails> txnList = new ArrayList<AccountDetails>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		try {
			txnList = fundTransferRepository.findAllByUpdatedDateLessThanEqualAndUpdatedDateGreaterThanEqual(
					formatter.parse(toDate), formatter.parse(fromDate));
		} catch (ParseException e) {
		}
		if (!txnList.isEmpty()) {
			for (AccountDetails txn : txnList) {
				if (txn.getAccountNumber().equals(accountNum)) {
					Account acc = new Account();
					acc.setAccountNumber(txn.getAccountNumber());
					acc.setTransactionType(txn.getTransactionType());
					acc.setAmount(txn.getAmount());
					acc.setUpdatedDate(txn.getUpdatedDate());
					accounts.add(acc);
				}
			}
		}
		return accounts;
	}

}
