package com.bank.bankapplication.service;

import java.util.List;

import com.bank.bankapplication.entity.Account;
import com.bank.bankapplication.entity.User;

public interface BankApplicationService {

	public void registerUser(User usr);

	public void addFunds(String accountNumber, String amount);

	public void transferFunds(String fromAccountNumber, String toAccountNumber, String amount);

	public List<Account> generateReport(String accountNum, String fromDate, String toDate);

}
