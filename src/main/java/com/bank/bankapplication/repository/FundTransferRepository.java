package com.bank.bankapplication.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bank.bankapplication.model.AccountDetails;
import com.bank.bankapplication.model.UserInfo;

@Repository
public interface FundTransferRepository extends JpaRepository<AccountDetails, String>{

	List<AccountDetails> findByAccountNumberOrderByAccountBalance(String fromAccountNumber);

	List<AccountDetails> findAllByUpdatedDateLessThanEqualAndUpdatedDateGreaterThanEqual(Date toDate, Date fromDate);

	List<AccountDetails> findByAccountNumberOrderByAccountBalanceDesc(String toAccountNumber);

}
