package com.bank.bankapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bank.bankapplication.model.UserInfo;

@Repository
public interface BankApplicationRepository extends JpaRepository<UserInfo, String>{

}
