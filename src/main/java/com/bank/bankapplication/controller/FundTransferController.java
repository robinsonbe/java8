package com.bank.bankapplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankapplication.service.BankApplicationService;

@RestController
@RequestMapping("/transferFund")
public class FundTransferController {

	@Autowired
	BankApplicationService bankApplicationService;

	@GetMapping("/addFunds")
	public String addFunds(@RequestParam String accountNumber, @RequestParam String amount) {
		if(Integer.valueOf(amount) <= 0)
			return "Please enter invalid amount";
		bankApplicationService.addFunds(accountNumber, amount);
		return "deposited";
	}

	@GetMapping("/transferFunds")
	public String transferFunds(@RequestParam String fromAccountNumber, @RequestParam String toAccountNumber,
			@RequestParam String amount) {
		if(Integer.valueOf(amount) <= 0)
			return "Please enter valid amount";
		if (fromAccountNumber.equals(toAccountNumber))
			return "From to To account number can not be same";
		bankApplicationService.transferFunds(fromAccountNumber, toAccountNumber, amount);
		return "transferred";
	}
}
