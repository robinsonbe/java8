package com.bank.bankapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankapplication.entity.Account;
import com.bank.bankapplication.service.BankApplicationService;

@RestController
@RequestMapping("/generateReport")
public class ReportGenerationController {

	@Autowired
	BankApplicationService bankApplicationService;
	
	@GetMapping("") 
	public List<Account> generateReport(@RequestParam String accountNum,@RequestParam String fromDate, @RequestParam String toDate){
		List<Account> result = bankApplicationService.generateReport(accountNum,fromDate, toDate);
		return result;	
	}
}
