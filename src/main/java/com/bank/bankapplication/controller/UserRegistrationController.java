package com.bank.bankapplication.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankapplication.entity.User;
import com.bank.bankapplication.service.BankApplicationService;

@RestController
@RequestMapping("/registerUser")
public class UserRegistrationController {
	
	@Autowired
	BankApplicationService bankApplicationService;

	@PostMapping("")
	public String saveEmployee(@RequestBody User usr) {
		int accountNumber = genAccountNumber();
		usr.setAccountNumber(String.valueOf(accountNumber).replaceAll("-", ""));
		bankApplicationService.registerUser(usr);
		return usr.getAccountNumber();
	}
	
	public int genAccountNumber() {
	    Random r = new Random(System.currentTimeMillis());
	    return 1000000000 + r.nextInt(2000000000);
	}
}
