package com.bank.bankapplication.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	@JsonAlias("first_name")
	private String firstName;
	
	@JsonAlias("last_name")
	private String lastName;
	
	@JsonProperty("location")
	private String location;
	
	@JsonAlias("identity_document_number")
	private String identityDocumentNumber;
	
	@JsonProperty("account_number")
	private String accountNumber;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getIdentityDocumentNumber() {
		return identityDocumentNumber;
	}

	public void setIdentityDocumentNumber(String identityDocumentNumber) {
		this.identityDocumentNumber = identityDocumentNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
}
